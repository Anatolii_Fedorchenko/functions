const getSum = (str1, str2) => {
  if((typeof(str1) !== 'string') || (typeof(str2) !== 'string')){
    return false;
  }

  if(!str1) str1 = '0';
  if(!str2) str2 = '0';

  const _isInvalidString = (str) => {
    for(let char of str){
      if(Number.isInteger(+char)) continue;
      else return true;  
    }
    return false;
  }

  if(_isInvalidString(str1) || _isInvalidString(str2)) return false;

  let result = [];
  let carry = 0;
  let bVal, lVal;
  if(str1.length > str2.length){
    bVal = str1.split('');
    lVal = str2.split('');
  } else {
    bVal = str2.split('');
    lVal = str1.split('');
  }

  for(let bdx = bVal.length - 1, ldx = lVal.length - 1; bdx >= 0; bdx--, ldx--){
    let sum;
    if(ldx >= 0){
      sum = +bVal[bdx] + +lVal[ldx] + carry;
      if (sum > 9) {
        carry = 1;
        sum = sum%10;
      } else {
        carry = 0;
      }
      result.unshift(sum);
    } else {
      sum = +bVal[bdx] + carry;
      if (sum > 9) {
        carry = 1;
        sum = sum%10;
      } else {
        carry = 0;
      }
      result.unshift(sum);
    }
  }

  if(carry) result.unshift(carry);

  return result.join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  
  let _authorStat = {
    Post: 0,
    comments: 0,
    toString(){ return `Post:${this.Post},comments:${this.comments}`;}
  }

  for(let post of listOfPosts){
    if(post.author === authorName){
      _authorStat.Post++;
    }
    if(post.comments){
      for(let comment of post.comments){
        if(comment.author === authorName){
          _authorStat.comments++;
        }
      }
    }
  }

  return _authorStat.toString();
};

const tickets=(people)=> {
  let _cashBox = {
    25: 0,
    50: 0,
    100: 0
  }
  for(let prop of people){
    switch(prop){
      case 25:
        _cashBox[prop]++;
        break;
      case 50:
        if(_cashBox[25]){
          _cashBox[prop]++;
          _cashBox[25]--;
        } else {
          return 'NO';
        }
        break;
      case 100:
        if(_cashBox[50] && _cashBox[25]){
          _cashBox[prop]++;
          _cashBox[50]--;
          _cashBox[25]--;
        }
        else if(_cashBox[25] > 2){
          _cashBox[prop]++;
          _cashBox[25] -= 3;
        } else {
          return 'NO';
        }
        break;
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
